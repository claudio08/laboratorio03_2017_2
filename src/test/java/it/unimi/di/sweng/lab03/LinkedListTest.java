package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list = new IntegerList();
	
	@Test
	public void firstTestToBeReplaced() {
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	
	@Test
	public void addLast() {
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
		list.addFirst(2);
		assertThat(list.toString()).isEqualTo("[2 1 3]");
		list.removeFirst();
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void illegalArgumentTest(){
		list.addLast("aaa");
	}
	
	@Test
	public void addFirst(){
		
	}
	
}
