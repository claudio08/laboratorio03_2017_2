package it.unimi.di.sweng.lab03;

public class IntegerList {
	
	IntegerNode head = null;
	IntegerNode tail = null;
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder("[");
		IntegerNode cur = head;
		while (cur!=null) {
			s.append(cur.getValue());
			cur = cur.next();
			if (cur!=null) s.append(" ");
		}
		return s.append(']').toString();
	}
	
	public void addLast(int n) {
		if (head == null)
			head = tail = new IntegerNode(n);
		else {
			IntegerNode node = new IntegerNode(n); 
			tail.setNext(node);
			tail = node;
		}
	}
	
	public void addLast(String n) {
		throw new IllegalArgumentException();
	}
	
	public void addFirst(int n) {
		if (head == null)
			head = tail = new IntegerNode(n);
		else {
			IntegerNode node = new IntegerNode(n); 
			node.setNext(head);
			head = node;
		}
	}
	
	public boolean removeFirst() {
		if (head == null) return false;
		head = head.next();
		return true;
	}
	
}
