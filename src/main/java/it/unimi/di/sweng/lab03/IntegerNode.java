package it.unimi.di.sweng.lab03;

public class IntegerNode {
	Integer value = null;
	IntegerNode next = null;
	
	public IntegerNode(int n) {
		value = n;
	}

	
	public IntegerNode next() {
		return next;
	}
	
	public void setNext(IntegerNode j) {
		next = j;
	}
	
	public int getValue() {
		return value;
	}
	
}
